import React from "react";
import {
  Typography,
  TextField,
} from "@material-ui/core";
import classes from './Common.module.css';

const StepOne = () => {
  return (
    <div className={classes.dataforms}>
      <Typography variant="h5"
      >
        Welcome! First things first...
      </Typography>
      <Typography className={classes.subheading}
      >
        You can always change them later.
      </Typography>
      <form className={classes.form}>
        <TextField className={classes.inputfield}
          label="Full Name"
          variant="outlined"
          placeholder="Steve Jobs"
        ></TextField>
        <TextField className={classes.inputfield}
          label="Display Name"
          variant="outlined"
          placeholder="Steve"
        ></TextField>
      </form>
    </div>
  );
};

export default StepOne;
