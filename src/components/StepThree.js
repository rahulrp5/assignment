import React from 'react';
import {
    Typography, Card, CardContent
  } from "@material-ui/core";
  import PeopleAlt from '@material-ui/icons/PeopleAlt';
  import Person from '@material-ui/icons/Person';
  import classes from './Common.module.css';

const StepThree = () => {
  return (
    <div className={classes.dataforms}>
      <Typography variant="h5"
      >
        How are you planning to use Eden?
      </Typography>
      <Typography className={classes.subheading}
      >
        We will streamline your setup experience accordingly.
      </Typography>
      <form className={classes.form}>
        <Card className={classes.card}>
          <Person/>
            <CardContent>
                <Typography className={classes.cardtitle}>For myself</Typography>
                <Typography className={classes.cardcontent}>Writr better. Think more clearly.Stay Organized</Typography>
            </CardContent>
        </Card>
        <Card className={classes.card}>
            <PeopleAlt/>
            <CardContent>
                <Typography className={classes.cardtitle}>With my team</Typography>
                <Typography className={classes.cardcontent}>Wikis,docs, tasks & projects all in one place.</Typography>
            </CardContent>
        </Card>
      </form>
    </div>
  )
}

export default StepThree