import React from 'react';
import {
    Typography,
    TextField,
  } from "@material-ui/core";
  import classes from './Common.module.css';

const StepTwo = () => {
  return (
    <div className={classes.dataforms}>
    <Typography 
      variant="h5"
    >
      Lets set up a home for all your work 
    </Typography>
    <Typography className={classes.subheading}
    >
      You can always create another workspace later.
    </Typography>
    <form className={classes.form}>
      <TextField className={classes.inputfield}
        label="Workspace Name"
        variant="outlined"
        placeholder="Eden"
      ></TextField>
      <TextField className={classes.inputfield}
        label="Workspace URL(optional)"
        variant="outlined"
        placeholder="Example"
      ></TextField>
    </form>
    </div>
  )
}

export default StepTwo