import React, { useState } from 'react';
import {Stepper, Step, StepLabel, Button} from '@material-ui/core';
import {
  Typography
} from "@material-ui/core";
import StepOne from './stepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import StepFour from './StepFour';
import classes from './Common.module.css';


const MultistepForm = () => {

  const [activeStep, setActiveStep] = useState(0);

   function getSteps(){
   return ["1", "2", "3", "4"] ;
  }
  
  const steps = getSteps();

  const handleNext = () =>{
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  } 

   function getStepsContent(stepIndex){
   switch(stepIndex){
     case 0:
       return <StepOne/>;
      case 1:
      return <StepTwo/>;
      case 2:
        return <StepThree/>;
        case 3:
        return <StepFour/>;
      default:
         // do nothing
   }
   }

  return (
    <div className={classes.multistepform}>
      <Typography variant="h5">Eden</Typography>
      <Stepper alternativeLabel activeStep={activeStep}>
        {steps.map(label=>(
      <Step key={label}>
        <StepLabel></StepLabel>
      </Step>
        ))}
      </Stepper>
      <>

     {getStepsContent(activeStep)}
      <Button
       onClick={handleNext}>
      {activeStep === (steps.length-1) ? "Launch Eden" : "Create Workspace"}
      </Button>
    
      </>
    </div>
  )
}

export default MultistepForm