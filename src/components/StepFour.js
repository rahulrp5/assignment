import React from 'react';
import {
    Typography
  } from "@material-ui/core";
  import CheckCircle from '@material-ui/icons/CheckCircle';
  import classes from './Common.module.css';

const StepFour = () => {
  return (
    <div className={classes.dataforms}>
      <CheckCircle/>
      <Typography variant="h5"
      >
        Congratulations, Eren!
      </Typography>
      <Typography className={classes.subheading}
      >
        You have completed onboarding, you can start using the Eden!
      </Typography>
      <form className={classes.form}>
      </form>
    </div>
  )
}

export default StepFour