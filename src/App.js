import React from 'react';
import './App.css';
import {BrowserRouter,Routes,Route} from 'react-router-dom';
import MultistepForm from './components/MultistepForm';

function App() {
  return (
    <BrowserRouter>
    <Routes>
        <Route path="/" element={<MultistepForm/>}/>
    </Routes>
       </BrowserRouter>
  );
}

export default App;
